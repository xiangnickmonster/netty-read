/*
 * Copyright 2015 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.handler.codec.protobuf;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.nano.CodedOutputByteBufferNano;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * An encoder that prepends the Google Protocol Buffers
 * <a href="https://developers.google.com/protocol-buffers/docs/encoding?csw=1#varints">Base
 * 128 Varints</a> integer length field. For example:
 * <pre>
 * BEFORE ENCODE (300 bytes)       AFTER ENCODE (302 bytes)
 * +---------------+               +--------+---------------+
 * | Protobuf Data |-------------->| Length | Protobuf Data |
 * |  (300 bytes)  |               | 0xAC02 |  (300 bytes)  |
 * +---------------+               +--------+---------------+
 * </pre> *
 *
 * @see CodedOutputStream
 * @see CodedOutputByteBufferNano
 */
@Sharable
public class ProtobufVarint32LengthFieldPrepender extends MessageToByteEncoder<ByteBuf> {

    @Override
    protected void encode(
            ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) throws Exception {
        int bodyLen = msg.readableBytes();
        int headerLen = computeRawVarint32Size(bodyLen);
        out.ensureWritable(headerLen + bodyLen);
        writeRawVarint32(out, bodyLen);
        out.writeBytes(msg, msg.readerIndex(), bodyLen);
    }

    /**
     * Writes protobuf varint32 to (@link ByteBuf).
     * @param out to be written to
     * @param value to be written
     *
     * 我们int占用4字节，但是通常大部分情况下都是很小的数字不需要4字节
     * 因为最大的值可能需要4字节，所以长尾效应，所以没办法必须用4字节的int来传输很小的数字
     *
     * 而varint32是长度可变的int，通过特殊编码，使得其表示一个int类型的数字最小1字节，最大5字节
     * 如果项目里都是很大的int，那不适合用varint32，如果大部分都是很小的int那么适合
     *
     * 看下他的编码规则，以 129 为例，它的二进制为 1000 0001，但是int是4个字节，所以他实际二进制编码为
     * 0000 0000 0000 0000 0000 0000 1000 0001
     * 可以看到有很多位造成了浪费，又因为最高位一般用于标记正负号，所以实际可存储数据的是7位。
     * 所以varint32的编码第一个字节后7位先存储129的后7位，也就是（000 0001），而129的第8位是1，也就是还少了1bit没有存储
     * 于是varint32的第一个字节的最高位用1来表示后续还有数据，所以varint32的第一个字节是（1000 0001）
     * 然后129的第8位剩余的1，用varint32的第二个字节来存储（000 0001），由于129位只剩下1bit了，所以varint32的第二个字节可以完全存储完
     * 所以varint32的第二个字节最高位设置0，表示后续没有数字了（0000 0001）
     * 所以varint32来存储129就是 0000 0001 1000 0001 ，也就是2个字节就存储了原先用int 4个字节表示的129
     *
     * 0x7F是 0111 1111
     * 0x80是 1000 0000
     */
    static void writeRawVarint32(ByteBuf out, int value) {
        while (true) {
            // 说明value实际小于1字节
            if ((value & ~0x7F) == 0) {
                out.writeByte(value);
                return;
            } else {
                // 说明value实际大于1字节
                // value & 0x7F表示获取到1~7位数据，再与0x80做或运算。
                //0x80表示1000 0000
                out.writeByte((value & 0x7F) | 0x80);
                value >>>= 7;
            }
        }
    }

    /**
     * Computes size of protobuf varint32 after encoding.
     * @param value which is to be encoded.
     * @return size of value encoded as protobuf varint32.
     */
    static int computeRawVarint32Size(final int value) {
        if ((value & (0xffffffff <<  7)) == 0) {
            return 1;
        }
        if ((value & (0xffffffff << 14)) == 0) {
            return 2;
        }
        if ((value & (0xffffffff << 21)) == 0) {
            return 3;
        }
        if ((value & (0xffffffff << 28)) == 0) {
            return 4;
        }
        return 5;
    }
}
